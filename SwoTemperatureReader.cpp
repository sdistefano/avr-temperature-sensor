//
// Created by Silvio Di Stefano on 03/03/2016.
//
#include <string.h>

#define dout TEMP_READER_PORT
#define pin TEMP_READER_P
#define din TEMP_READER_PIN
#define ddr TEMP_READER_DDR

struct TemperatureReading {
    int temperature;
    int humidity;
};

class SwoTemperatureReader {
private:
    void pull_down() {
        dout &= ~(1 << pin);
    }

    void pull_up() {
        dout |= (1 << pin);
    }

    void wait_for_sensor_response() {
    }

public:
    SwoTemperatureReader() {
    }
    void send_start_signal() {
        //take note of current time
        //reset port
        ddr |= (1 << pin);
        dout |= (1 << pin);
        _delay_ms(100);
        //send request
        log_line("starting init sequence");
        pull_down();
        _delay_ms(18);
        pull_up();
        _delay_us(1);
        ddr &= ~(1 << pin);
        dout |= (1 << pin);
        loop_until_bit_is_clear(din, pin);
        loop_until_bit_is_set(din, pin);
    }

    bool read_bit() {
        loop_until_bit_is_set(din, pin);
        _delay_us(30);
        if (bit_is_clear(din, pin)) {
            return 0;
        } else {
            loop_until_bit_is_clear(din, pin);
            return 1;
        }
    }
    TemperatureReading receive_transmission() {
        //read the data
        char bits[5];
        uint8_t i,n;
//        uint8_t j,i;

        loop_until_bit_is_clear(din, pin);


        for (i=0; i<5; i++) {
            char result = 0;
            for (n=0; n<8; n++) {
                loop_until_bit_is_set(din, pin);
                _delay_us(28);
                if (bit_is_clear(din, pin)) {
                    continue;
                }
                result |= (1 << (7-n));
                loop_until_bit_is_clear(din, pin);
            }
            bits[i] = result;
        }

        TemperatureReading reading;

        if (bits[0] + bits[1] + bits[2] + bits[3] == bits[4]) {
            log_line("Checksum correct");
        } else {
            log_line("Error reading temperature!");
        }
        reading.temperature = bits[2];
        reading.humidity = bits[0];

        return reading;
    }
};
