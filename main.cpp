#include <stdio.h>
#include <stdlib.h>
#include <avr/io.h>
#include <avr/power.h>
#include <uart.c>
#include <logging.c>
#include <util/delay.h>
#include <interrupt.h>

#define TEMP_READER_DDR DDRB
#define TEMP_READER_PORT PORTB
#define TEMP_READER_PIN PINB
#define TEMP_READER_P PB1
#include "SwoTemperatureReader.cpp"


int main() {
    SwoTemperatureReader data_reader = SwoTemperatureReader();
    uart_init();
    FILE usart_str = FDEV_SETUP_STREAM(uart_putchar, uart_getchar, _FDEV_SETUP_RW);
    stdin=stdout=stderr = &usart_str;

    log_line("Starting...");

    DDRB = 0xff;
    while (1) {
        cli();
        data_reader.send_start_signal();
        TemperatureReading reading = data_reader.receive_transmission();
        sei();
        log_line("%d degrees celsius", reading.temperature);
        _delay_ms(1e3);
    }
    return 0;
}